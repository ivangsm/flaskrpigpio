# FlaskRPiGPIO

Control basico de GPIO sobre una Raspberry Pi utilizando **Flask** y **RPi.GPIO**

He usado DietPi como mi sistema operativo, las siguientes instrucciones deberian funcionar para cualquier distro para RPi basada en Debian

Necesitas instalar varios paquetes para poder ejecurta la app

Python3 PIP

`sudo apt install python3-minimal python3-pip`

Flask RPi.GPIO

`sudo pip3 install flask RPi.GPIO`

Para inciar el servidor

`sudo python3 app.py`