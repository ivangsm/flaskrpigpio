import RPi.GPIO as GPIO
from flask import Flask, render_template, request
app = Flask(__name__)

GPIO.setmode(GPIO.BCM)

# Crea un diccionario llamado "pins" para almacenar el numero, nombre y estado de cada pin.
pins = {
   23 : {'name' : 'GPIO 23', 'state' : GPIO.LOW},
   24 : {'name' : 'GPIO 24', 'state' : GPIO.LOW}
   }

# Inicializa los pines en modo de salida (OUT) y los apaga (LOW)
for pin in pins:
   GPIO.setup(pin, GPIO.OUT)
   GPIO.output(pin, GPIO.LOW)

@app.route("/")
def main():
   # Para cada pin lee el estado y lo almacena en el diccionario de pins
   for pin in pins:
      pins[pin]['state'] = GPIO.input(pin)
   # Pone el diccionario de pines en el diccionario "templateData"
   templateData = {
      'pins' : pins
      }
   # Pasa los datos del diccionario dentro de index.html y los retorna el usuario
   return render_template('index.html', **templateData)

# La siguiente funcion se ejecuta cuando alguien hace una peticion a una URL con el numero de pin y la accion en ella
@app.route("/<changePin>/<action>")
def action(changePin, action):
   # Convierte el pin de la peticion en entero
   changePin = int(changePin)
   # Obtiene el nombre del dispositivo para el pin a cambiar
   deviceName = pins[changePin]['name']
   if action == "on":
      # Enciende PIN
      GPIO.output(changePin, GPIO.HIGH)
      # Guada el estatus para pasarlo al diccionario
      message = "Turned " + deviceName + " on."
   if action == "off":
      GPIO.output(changePin, GPIO.LOW)
      message = "Turned " + deviceName + " off."

   #Para cada pin lee el estado y lo guarda en el diccionario
   for pin in pins:
      pins[pin]['state'] = GPIO.input(pin)

   templateData = {
      'pins' : pins
   }

   return render_template('index.html', **templateData)

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=8080, debug=True)